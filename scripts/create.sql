-- Table: public.users

DROP TABLE public.users;

CREATE TABLE public.users
(
    user_id integer NOT NULL DEFAULT nextval('users_user_id_seq'::regclass),
    user_name character varying(32) COLLATE pg_catalog."default" NOT NULL,
    pass_hash bytea NOT NULL,
    pass_salt bytea NOT NULL,
    CONSTRAINT users_pkey PRIMARY KEY (user_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.users
    OWNER to postgres;

-- Table: public.permissions

DROP TABLE public.permissions;

CREATE TABLE public.permissions
(
    permission_id smallint NOT NULL DEFAULT nextval('permissions_permission_id_seq'::regclass),
    permission_name character varying(16) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT permissions_pkey PRIMARY KEY (permission_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.permissions
    OWNER to postgres;

-- Table: public.user_permission

DROP TABLE public.user_permission;

CREATE TABLE public.user_permission
(
    user_id integer NOT NULL,
    permission_id smallint NOT NULL,
    CONSTRAINT user_permission_pkey PRIMARY KEY (user_id, permission_id),
    CONSTRAINT permission_id FOREIGN KEY (permission_id)
        REFERENCES public.permissions (permission_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT user_id FOREIGN KEY (user_id)
        REFERENCES public.users (user_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.user_permission
    OWNER to postgres;