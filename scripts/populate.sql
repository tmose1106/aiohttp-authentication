-- Table: public.permissions

INSERT INTO public.permissions(
	permission_id, permission_name)
	VALUES (0, 'protected'),
        (1, 'public');
