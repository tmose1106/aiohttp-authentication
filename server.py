import base64
from hashlib import pbkdf2_hmac
from http import HTTPStatus
from os import urandom

from aiohttp import web
from aiohttp_security import setup as setup_security
from aiohttp_security import SessionIdentityPolicy
from aiohttp_security import (
    remember, forget, authorized_userid,
    check_permission, check_authorized,
)
from aiohttp_security.abc import AbstractAuthorizationPolicy
from aiohttp_session import setup as setup_session
from aiohttp_session.cookie_storage import EncryptedCookieStorage
import asyncpg
from cryptography import fernet


class DBAuthorizationPolicy(AbstractAuthorizationPolicy):
    def __init__(self, dbengine):
        self.dbengine = dbengine

    async def authorized_userid(self, identity):
        statement = await self.dbengine.prepare(
            '''SELECT user_id FROM public.users WHERE user_name = $1;''')

        if await statement.fetchval(identity):
            return identity
        else:
            return None

    async def permits(self, identity, permission, context=None):
        if identity is None:
            return False

        statement = await self.dbengine.prepare('''
SELECT t_p.permission_name FROM public.users t_u
INNER JOIN public.user_permission t_up ON t_up.user_id = t_u.user_id
INNER JOIN public.permissions t_p ON t_p.permission_id = t_up.permission_id
WHERE t_u.user_name = $1;''')

        permission_name = await statement.fetchval(identity)

        if permission_name and permission_name == permission:
            return True

        return False


def hash_with_salt(password, salt):
    return pbkdf2_hmac('sha512', password.encode(), salt, 100000)


async def handler(request):
    user = await authorized_userid(request)

    if user:
        return web.Response(text='User {0} authorized'.format(user))
    else:
        return web.HTTPFound(request.app.router['login'].url_for())


async def login_page(request):
    return web.FileResponse('static/index.html')


async def logout_page(request):
    await check_authorized(request)
    response = web.Response(text='You have been logged out')
    await forget(request, response)
    return response


async def internal_page(request):
    await check_permission(request, 'public')
    response = web.Response(
        text='This page is visible for all registered users')
    return response


async def protected_page(request):
    await check_permission(request, 'protected')
    response = web.Response(text='This page is protected')
    return response


async def check_credentials(db_engine, username, password):
    statement = await db_engine.prepare(
        '''SELECT user_id, pass_hash, pass_salt FROM public.users WHERE user_name = $1;''')

    record = await statement.fetchrow(username)

    if not record:
        return False

    user_id, key, salt = record['user_id'], record['pass_hash'], record['pass_salt']

    return hash_with_salt(password, salt) == key


async def login(request):
    router = request.app.router
    form = await request.post()

    username, password = form['name'], form['password']

    if not await check_credentials(request.app['pg_engine'], username, password):
        return web.Response(text='Invalid credentials', status=HTTPStatus.FORBIDDEN)

    response = web.HTTPFound(router['restricted'].url_for())

    await remember(request, response, username)

    return response


async def register(request):
    router = request.app.router
    
    form = await request.post()
    
    username = form['username']

    statement = await request.app['pg_engine'].prepare(
        '''SELECT EXISTS(SELECT 1 FROM users where user_name = $1);''')

    name_used = await statement.fetchval(username)

    if name_used:
        return web.Response(
            text=f'User \'{username}\' already exists', status=HTTPStatus.FORBIDDEN)

    password = form['password']
    password_length = len(password)

    if not password or password_length < 3 or password_length > 16:
        return web.Response(
            text=f'Password doesn\'t meet required criteria', status=HTTPStatus.FORBIDDEN)

    salt = urandom(32) # Generate some random bytes

    key = hash_with_salt(password, salt)

    query = '''INSERT INTO public.users (user_name, pass_hash, pass_salt) VALUES ($1, $2, $3);'''

    await request.app['pg_engine'].execute(query, username, key, salt)

    return web.Response(text='User registered successfully', status=HTTPStatus.FORBIDDEN)


async def setup_app(app):
    # secret_key must be 32 url-safe base64-encoded bytes
    fernet_key = fernet.Fernet.generate_key()
    secret_key = base64.urlsafe_b64decode(fernet_key)

    setup_session(app, EncryptedCookieStorage(secret_key))
    
    # Gets rest of credentials from passfile
    app['pg_engine'] = await asyncpg.connect(database='test')

    setup_security(app, SessionIdentityPolicy(), DBAuthorizationPolicy(app['pg_engine']))

    yield

    # On cleanup
    await app['pg_engine'].close()


def make_app():
    app = web.Application()

    app.add_routes([
        web.get('/', handler, name='restricted'),
        web.get('/login', login_page, name='login'),
        web.post('/login', login),
        web.post('/register', register),
        web.get('/logout', logout_page, name='logout'),
        web.get('/public', internal_page, name='public'),
        web.get('/protected', protected_page, name='protected'),
    ])

    app.cleanup_ctx.append(setup_app)

    return app

if __name__ == '__main__':
    web.run_app(make_app())
